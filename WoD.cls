\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{WoDTeX}[A class to replicate World of Darkness RPG books]

\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{book}
}

% load full WoDTeX directory
\def\input@path{{wwtex/}{images}}

\newif\ifvamp
\vampfalse
\newif\ifDA
\DAfalse
\newif\ifcainite
\cainitefalse

\DeclareOption{Vampire}{
  \providecommand{\genrePath}{Vampire}
  \providecommand{\genreFrontTextColor}{black!10}
  \renewcommand{\genreGeometryBottom}{2.3cm}
  \renewcommand{\genreGeometryUpper}{2.9cm}
  \renewcommand{\genreGeometryInner}{1.5cm}
  \providecommand{\genreGeometryOuter}{2.5cm}
  \vamptrue
}

\DeclareOption{DA}{
  \DAtrue
  \renewcommand{\genreGeometryBottom}{3.5cm}
  \renewcommand{\genreGeometryInner}{1.7cm}
}

\providecommand{\genreGeometryBottom}{3cm}
\providecommand{\genreGeometryInner}{1.5cm}
\providecommand{\genreGeometryOuter}{2cm}
\providecommand{\genreGeometryUpper}{1.8cm}

%--------------------

\ProcessOptions\relax

% Now we execute any options passed in
\LoadClass[openany,a4paper]{book}

\RequirePackage[table]{xcolor}
\ifDA\else
  \rowcolors{3}{lightgray}{}
\fi

\RequirePackage[
pdfstartpage=1,
hidelinks=true,
bookmarks=true]%
{hyperref}

\RequirePackage[many]{tcolorbox}
  \tcbuselibrary{vignette}
  \usetikzlibrary{decorations.pathmorphing}% already got tikz through tcolorbox. Adding libraries for fancy storyTime boxes.
  \usetikzlibrary{shadows}

\RequirePackage{enumitem} % just to left-align dots
\RequirePackage{needspace}
\RequirePackage{fancyhdr}
\RequirePackage[section=section]{glossaries-extra}
\RequirePackage{glossary-mcols}
\RequirePackage{imakeidx}
  \indexsetup{level=\section}
\RequirePackage{wrapfig}
\RequirePackage{xstring}
\RequirePackage{multicol}
\RequirePackage{microtype}
\RequirePackage{graphicx}
\RequirePackage{array,colortbl,tabularx,multirow}
  \newcolumntype{Y}{>{\centering\arraybackslash}X}
\RequirePackage{mathabx} % for agg-damage symbols, maybe remove later
\RequirePackage[a4paper, nomarginpar, top=\genreGeometryUpper, bottom=\genreGeometryBottom, outer=\genreGeometryOuter, inner=\genreGeometryInner]{geometry}

\newcommand\daggerFoot[1]{\textsuperscript{$\dagger$}\footnotetext{\color{footcolor}$\dagger$ #1}}

\newcommand\skillDot{$\newmoon$}
\newcommand\smallDot{\begin{tiny}$\newmoon$\end{tiny}}
\newcommand\ruleDot{\par\smallDot~}
\newcommand\openDot{$\fullmoon$}
\newcommand\emptyBox{$\boxvoid$}
\newcommand\slashedBox{$\boxslash$}
\newcommand\lethalBox{$\boxtimes$}
\newcommand\aggBox{$\boxasterisk$}


%%%%% Background %%%%%

\RequirePackage{svg}
\svgpath{wwtex/images/}
\RequirePackage{background}

\ifDA
  \providecommand\bgimgodd{da_odd}
  \providecommand\bgimgeven{da_even}
\else
  \ifvamp
    \providecommand\bgimgodd{vampBG}
    \providecommand\bgimgeven{vampBG}
  \else
    \providecommand\bgimgodd{WoDBG}
    \providecommand\bgimgeven{WoDBG}
  \fi
\fi

\backgroundsetup{
scale=1,
color=black,
opacity=1,
angle=0,
contents={%
  \ifvamp
    \ifodd\value{page}
      \includesvg[height=1\paperheight]{\bgimgodd}
    \else
      \includesvg[height=1\paperheight]{\bgimgeven}
    \fi
  \else
    \ifodd\value{page}
      \includesvg[width=2.4\textwidth,height=\paperheight]{\bgimgodd}
    \else
      \includesvg[width=2.4\textwidth,height=\paperheight]{\bgimgeven}
    \fi
  \fi
  }%
}

%%%%% System  %%%%%
% This lets us type "Here in \setting\ we roll D10's".

\ifvamp
  \ifDA
    \newcommand\setting{Dark Ages}
    \newcommand\longSetting{Dark Ages: Vampire}
  \else
    \newcommand\setting{Vampire}
    \newcommand\longSetting{Vampire: The Masquerade}
  \fi
\else
  \ifDA
    \newcommand\setting{Dark Ages}
    \newcommand\longSetting{Dark Ages}
  \else
    \newcommand\setting{World of Darkness}
    \newcommand\longSetting{The World of Darkness}
  \fi
\fi

%%%%% Page Setup %%%%%

%%% Footnotes %%%

% fnsymbol only has 9 symbols
\RequirePackage[perpage]{footmisc}
\renewcommand{\thefootnote}{\fnsymbol{footnote}}

\setlength{\parskip}{0pt}

\setcounter{secnumdepth}{0}

\fancyfoot{}
\fancyhead{}
\renewcommand\headrulewidth{0pt}
\pagestyle{fancyplain}

\ifDA
  
  \setlength\footskip{30pt}
  
  \fancyfoot[RO]{\large\sectionfont \leftmark\phantom{.}\\\vspace{2em}{\large\thepage}}
  \fancyfoot[LE]{\large\sectionfont \@title\phantom{.}\\\vspace{2em}{\large\thepage}}
\else
  \setlength\footskip{20pt}
  \fancyfoot[CO]{\headingfont \leftmark\phantom{.}\\\vspace{.3em}{\thepage}}
  \fancyfoot[CE]{\headingfont \@title\phantom{.}\\\vspace{.3em}{\thepage}}
\fi

\RequirePackage[explicit,raggedright]{titlesec}
\RequirePackage{titletoc}
\RequirePackage{appendix}
\RequirePackage{fontspec}
\RequirePackage{fmtcount}% makes chapter numbers into words

\ifDA
  \newfontfamily\headingfont[
  Path = wwtex/fonts/,
  Extension = .ttf,
  ]{LucidiaBlackletter}%
  \newfontfamily\sectionfont[
  Path = wwtex/fonts/,
  Extension = .ttf,
  ]{deroos}%
\else
  \ifvamp%
    \newfontfamily\headingfont[
    Path = wwtex/fonts/,
    Extension = .ttf,
    ]{percolator-expert}%
    \newfontfamily\sectionfont[
    Path = wwtex/fonts/,
    Extension = .ttf,
    ]{percolator-expert}%
  \else%
    \newfontfamily\headingfont[
    Path = wwtex/fonts/,
    Extension = .ttf,
    ]{Delavan}%
    \newfontfamily\sectionfont[
    Path = wwtex/fonts/,
    Extension = .ttf,
    ]{Delavan}%
  \fi
\fi

\newfontfamily\writingfont[
Path = wwtex/fonts/,
Extension = .ttf,
]{percolator}%

\setmainfont[
  Path = wwtex/fonts/,
  Extension = .ttf,
  UprightFont = *-regular,
  BoldFont = *-bold,
  %AutoFakeSlant = 0.6,
  ItalicFont = *-italic,
  BoldItalicFont = *-bold-italic
]{goudy-old-style}

\renewcommand{\chapterautorefname}{Chapter}

\titleformat{\chapter}[display]
  {\filright\headingfont\Huge\bfseries}
  {}
  {0pt}
  {%
    \renewcommand{\thechapter}{\NUMBERstring{chapter}}
    \ifDA
    \MakeUppercase
      \chaptertitlename~\thechapter
      \vspace{1em}
    \\{\tcbfontsize{6}#1}
    \else
    \begin{tcolorbox}[
      enhanced,
      watermark graphics=images/marble.png,
      watermark overzoom = 1.0,
      no borderline,
      finish vignette={size=2mm},
      height = 9cm,
      coltext = white,
      halign  = center,
      valign = center,
      no borderline,
      finish vignette={size=1mm},
      opacityupper   = 0.7,
    ]
    \MakeUppercase
      \chaptertitlename~\thechapter
      \vspace{1em}
    \\{\tcbfontsize{6}#1}
    \end{tcolorbox}%
    \fi
  }[\renewcommand{\thechapter}{\numberstring{chapter}}]
% The 'chapter' counter gets used in the book with \autoref, so it needs reset to normal after use.


\titlespacing*{\chapter}
  {0pt}{-60pt}{0pt}

\ifDA
  \titleformat{\section}{\needspace{6em}\filright\Huge\sectionfont\bfseries}{}{1em}{#1}
  \titleformat{\subsection}{\needspace{6em}\filright\LARGE\sectionfont}{}{0em}{#1}
  \titleformat{\subsubsection}{\filright\needspace{3em}\large\sectionfont}{}{0em}{#1}
\else
  \titleformat{\section}{\titlerule[1.5pt]\Huge\headingfont\bfseries}{}{1em}{#1}
  \titleformat{\subsection}{\LARGE\headingfont}{}{0em}{#1}
  \titleformat{\subsubsection}{\needspace{3em}\Large\headingfont}{}{0em}{#1}
\fi


\titleformat{\paragraph}[runin]%
{\bfseries}{}{0pt}{\raisebox{2pt}{\smallDot} #1}

%%%%% Commands %%%%%

% The \Repeat command repeats another thing, like \Repeat{3}{\skillDot} makes 3 dots.

\newcommand{\Repeat}[1]{%
    \expandafter\@Repeat\expandafter{\the\numexpr #1\relax}%
}

\def\@Repeat#1{%
    \ifnum#1>0
        \expandafter\@@Repeat\expandafter{\the\numexpr #1-1\expandafter\relax\expandafter}%
    \else
        \expandafter\@gobble
    \fi
}
\def\@@Repeat#1#2{%
    \@Repeat{#1}{#2}#2%
}

% And need to fix Table of Contents, otherwise it shows an ugly 'Chapter  0'

\setcounter{tocdepth}{0}
\renewcommand{\thepart}{\Roman{part}}
\renewcommand*\contentsname{Contents}
\renewcommand\tableofcontents{%
  \begin{centering}
  {\headingfont\unskip\tcbfontsize{5}{\@title}\strut\par}

  \vspace{1em}
  \sectionfont\Large Contents

  \Large\sectionfont\@starttoc{toc}
  \end{centering}
}

\newcommand{\SkillDots}[1]{%
    \Repeat{#1}{\skillDot}
}%

\newcounter{emptyDots}

\newcommand{\dottedSkill}[2][5]{%
  \setcounter{emptyDots}{#1}%
  \addtocounter{emptyDots}{-#2}%
  \Repeat{#2}{\skillDot}\Repeat{\value{emptyDots}}{\openDot}
}%

\newcommand{\healthBoxes}[2][0]{%
  \setcounter{emptyDots}{#2}%
  \addtocounter{emptyDots}{-#1}%
  \Repeat{\value{emptyDots}}{\emptyBox\ }\ifnum#1>#2\else\Repeat{#1}{\slashedBox\ }\fi%
}%

\newcounter{traitNo}
\setcounter{traitNo}{1}
\newcounter{successNo}
\setcounter{successNo}{1}

\newcommand\trait[2][1]{%
  \ifnum#1=1
    \item[\Repeat{\value{traitNo}}{\smallDot}] #2
  \else
    \item[X] #2
    \setcounter{traitNo}{0}
  \fi
  \stepcounter{traitNo}%
}

\newcommand\powerLevel[2][0]{%
  \ifnum#1=0%
  \else%
    \setcounter{traitNo}{1}
  \fi
  \subsubsection[#2]{\begin{footnotesize}\Repeat{\value{traitNo}}{\skillDot}\end{footnotesize} #2}
  \index{#2}
  \stepcounter{traitNo}
}

\newlist{dotted}{description}{1}

\newenvironment{traitList}[1][4]{
  \setlist[dotted]{style=multiline,topsep=1em,leftmargin=#1em,font=\textbf,align=parleft}
  \needspace{2em}
  \setcounter{traitNo}{1}
  \begin{dotted}
}{
  \end{dotted}
  \needspace{2em}
}

\newcommand\newTrait[1]{
  \subsubsection{#1}
  \index{#1}
}

\newcommand\showSucc[1]{%
  \ifnum\value{successNo}<0
    \item[Botch] 
  \else
    \ifnum\value{successNo}<1
      \item[Failure] 
    \else
      \ifnum\value{successNo}<2
        \item[1 success] 
      \else%
        \item[\arabic{successNo} successes] %
      \fi%
    \fi
  \fi
   #1 \\%
  \stepcounter{successNo}%
}

\newenvironment{successList}[1][1]{
  \setlist[dotted]{style=multiline,topsep=1em,leftmargin=6em,font=\normalfont,align=parleft}
  \setcounter{successNo}{#1}
  \begin{dotted}
}{
  \end{dotted}
}

%%% Shaded Box %%%

\definecolor{tableShade}{gray}{0.9}

\definecolor{footcolor}{gray}{0.15}

\definecolor{shadecolour}{gray}{0.15}

\tcbsetforeverylayer{
    fonttitle   = {\sectionfont\Large},
    coltitle    = black,
    sharp corners,
    breakable,
    before skip = 0.5cm,
    after skip  = 0.5cm,
    borderline  = {1.5pt}{-2pt}{black},
    titlerule   = 20pt,
    center title,
    every float =\centering,
    code       = {\needspace{2em}\renewcommand{\thempfootnote}{\fnsymbol{mpfootnote}}},
}   

\ifDA
  \tcbsetforeverylayer{
    borderline = {0.3mm}{0.2mm}{black},
    borderline = {0.3mm}{-1.3mm}{black},
    boxsep = 3mm,
    bottomtitle = -3em,
  }
\else
  
  \tcbsetforeverylayer{
      blank,
      colbacktitle = white,
      coltitle  = black,
      colback = white,
      colframe = black,
      boxrule = 3pt,
  }
\fi

\newtcolorbox{tbox}[1][]{%
  title=#1,
  blank,
  boxsep = 3mm,
  code={\needspace{5em}},
}

\newtcolorbox{sideBox}[1][]{%
  title=#1,
  blank,
  boxsep=3mm,
  parbox=false,
}

\ifDA
  \newtcolorbox{edNote}[1][]{%
    enhanced,
    title=#1,
    skin    = enhanced,
    parbox=false,
    no borderline,
    rounded corners,
    colback = black!75!white,
    colframe = black!75!white,
    colbacktitle = black!75!white,
    coltitle = white,
    coltext = white,
    code = {\bfseries},
  }
\else
  \newtcolorbox{edNote}[1][]{%
    title=#1,
    parbox=false,
  }
\fi

\newtcolorbox{lowbox}[1][]{%
  title=#1,
  blank,
  boxsep=3mm,
  float*=htb!,
  parbox=false,
  width =\textwidth,
}

\newtcolorbox{bigTable}[2][@{}XXX@{}]{
  tabularx = {#1},
  blank,
  title    = #2,
  width    =\textwidth,
  float*=htb,
  boxsep=3mm,
}

\newtcolorbox{boxTable}[2][XXX]{
  tabularx  = {#1},
  blank,
  title     = #2,
}

\newtcolorbox{playText}[1][]{%
  blank,
  boxsep=3mm,
  parbox=false,
  watermark graphics = wwtex/images/marble.png,
  watermark overzoom = 1.0,
  watermark opacity = 0.3,
  no borderline,
  drop fuzzy shadow=gray,
}

\newcommand\sidePic[1]{
  \noindent
  \IfFileExists{images/#1.svg}{
    \includesvg[width=\textwidth]{images/#1}
  }{
    \includegraphics[width=\linewidth]{images/#1.jpg}
    \raggedbottom
  } 
}

\newcommand\pic[1]{%
  \begin{figure*}[b!]
    \IfFileExists{images/#1.svg}{
      \includesvg[width=\textwidth]{images/#1}
    }{
      \includegraphics[width=\textwidth]{images/#1.jpg}
   } 
  \end{figure*}
}

\newenvironment{example}[1][]{%
  #1
  \begin{itshape}
}{
  \end{itshape}
}

\newenvironment{torn}[1][]{
\tcolorbox[
    enhanced jigsaw, breakable,
    no borderline,
    frame hidden,
    width  = \linewidth-30pt,
    valign = center,
    boxsep = 4pt,
    overlay={%
        \draw [
            draw=white!50!black,% border colour
            fill=black!10!white,
            decorate, % decoration
            decoration={random steps,segment length=4pt,amplitude=3pt},
            drop shadow, % shadow
        ]
        (frame.north west)--(frame.north east)--
        (frame.north east)--(frame.south east)--
        (frame.south east)--(frame.south west)--
        (frame.south west)--(frame.north west);
    },
    parbox=false,#1
]
}{
\endtcolorbox
}


\newenvironment{storyTime}{%
  \clearpage
  \begin{torn}[height fill]
  \Large\begin{writingfont}
  \par\vfill
}{
  \par\vfill
  \end{writingfont}
  \end{torn}
}


\newenvironment{letter}{%
  \begin{torn}
  \Large\begin{writingfont}
}{
  \end{writingfont}
  \end{torn}
}

\newcommand\topQuote[1]{\begin{center}\begin{sectionfont}#1\end{sectionfont}\end{center}}

\newenvironment{sideNote}{}{}

\newcommand\characterQuote[1]{
  \footnotetext{Quote: \textit{#1}}
}

%%%%% Side Quests %%%%%

% Name of the current side quest area.
\newcommand\sqAreaName{}
\newcommand\sqAreaList{}

% Now the chapter on 'the Slums' (or whatever) receives a default area where the Side Quests take place.
\newcommand\sqArea[1]{
  \stopcontents[sq]
  \renewcommand\sqAreaName{#1}
  \resumecontents[#1]
  \startcontents[sq]
}

% Start tracking minitocs for later use with stories ('side quests').
\newcommand\declareAreas[1][City Centre, Suburbs, Barrens, Slums, Elysium]{
  \renewcommand\sqAreaList{#1}
  \startcontents[sq]
  \stopcontents[sq]
  \foreach \a in \sqAreaList {\startcontents[\a]}
  \foreach \a in \sqAreaList {\stopcontents[\a]}
}

\newcommand\sideQuest[2][\sqAreaName]{
  \stopcontents[sq]
  \startcontents[sq]
  \foreach \s in {#1}{\resumecontents[\s]}
  \section{#2}
  \foreach \s in {#1}{\stopcontents[\s]}
  \begin{lowbox}
  \printcontents[sq]{l}{2}{\subsubsection*{#2}\setcounter{tocdepth}{2}}
  \end{lowbox}
}

% Stop title indentation in SQ summaries
\titlecontents{subsection}
  [0pt]
  {\vspace*{\baselineskip}\mdseries}
  {\contentslabel[\thecontentslabel]{2.5em}}
  {}
{\hfill\thecontentspage}
  []

\newcommand\sqPart[3][\sqAreaName]{
  \resumecontents[#1]
  \subsection[#2 -- #3]{#2\\\begin{footnotesize}-- #1\end{footnotesize}}
  \stopcontents[#1]
}


\newcommand\sqSummary{
  \foreach \a in \sqAreaList{\printcontents[\a]{l}{1}{\subsection*{\a}\setcounter{tocdepth}{2}}}
}


%%%%% Character Boxes %%%%%

% Making dynamic boxes can be a pain.  We start with making one variable per Trait.

\newcommand\characterName

\newcommand\STR{2}
\newcommand\DEX{2}
\newcommand\STM{2}
\newcommand\CHA{2}
\newcommand\MAN{2}
\newcommand\APP{2}
\newcommand\PRC{2}
\newcommand\INT{2}
\newcommand\WTS{2}

\newcommand\ABIL{}
\newcounter{WIL}
\setcounter{WIL}{2}
\newcommand\NAT{}
\newcommand\DEM{}
\newcommand\EQUIP{}
\newcommand\WPN{}
\newcommand\CLT{}
\newcommand\BACK{}

\newcommand\CLAN{}
\newcommand\GEN{13}
\newcommand\SIRE{}
\newcommand\ROAD{}
\newcommand\ROADRT{}
\newcommand\HUM{}
\newcommand\CON{}
\newcommand\SLF{}
\newcommand\CRG{}
\newcommand\DISC{}


% One the character's done, we'll have to clean up those variables, because the box displays things based on which variables are empty.  We don't need to clean up Strength and Willpower, because those will always be set, and always be displayed.

\newcommand\cleanupVars{
  \renewcommand\CHA{}
  \renewcommand\MAN{}
  \renewcommand\APP{}
  \renewcommand\PRC{}
  \renewcommand\INT{}
  \renewcommand\WTS{}

  \renewcommand\ABIL{}
  \renewcommand\NAT{}
  \renewcommand\DEM{}
  \renewcommand\CLT{}
  \renewcommand\EQUIP{}
  \renewcommand\BACK{}
  \renewcommand\SIRE{}
  \cainitefalse
}

% Some things, like Charisma, don't display with mindless things like animals.

\newif\ifmind
\mindfalse

% Latex only handles 9 variables, so we lump the commands into sub-commands which set many commands at a time.

\newcommand\body[3]{
  \renewcommand\STR{#1}
  \renewcommand\DEX{#2}
  \renewcommand\STM{#3}
}

\newcommand\mind[7]{

  \mindtrue
  \renewcommand\CHA{#1}
  \renewcommand\MAN{#2}
  \renewcommand\APP{#3}
  \renewcommand\PRC{#4}
  \renewcommand\INT{#5}
  \renewcommand\WTS{#6}
  \setcounter{WIL}{#7}

}

\newcommand\animind[3]{
  \mindfalse
  \renewcommand\PRC{#1}
  \renewcommand\WTS{#2}
  \setcounter{WIL}{#3}
}

\newcommand\archetypes[2][]{
  \renewcommand\NAT{#1}
  \renewcommand\DEM{#2}
}

\newcommand\setRoad[2][Humanity]{
  \renewcommand\ROAD{#1}
  \renewcommand\ROADRT{#2}
}

\newcommand\generation[2][]{
  \renewcommand\SIRE{#1}
  \renewcommand\GEN{#2}
}

\newcommand\backgrounds[2][]{
  \renewcommand\CLT{#1}
  \renewcommand\BACK{#2}
}

\newcommand\equipment[2][]{
  \renewcommand\WPN{#1}
  \renewcommand\EQUIP{#2}
}

\newcommand\vampireTraits[7]{
  \renewcommand\CLAN{#1}
  \generation#2
  \setRoad#3
  \renewcommand\CON{#4}
  \renewcommand\SLF{#5}
  \renewcommand\CRG{#6}
  \renewcommand\DISC{#7}
}

\newcommand\characterBox{

  \begin{tbox}[{\characterName}]
  
  % The table really pushes to the right, so we define the
  % space as nothing with '@'.
  
  \ifcainite%
    \begin{tabularx}{\textwidth}{@{}XXX@{}}
    \hiderowcolors
    \textbf{Clan:} \CLAN & \textbf{Generation:} \GEN &  \expandafter\ifx\SIRE\empty\else\textbf{Sire:} \SIRE\fi \\
    \end{tabularx}
  \fi
  \begin{tabularx}{\textwidth}{@{}lXlXlX@{}}
  \hiderowcolors
  
  \ifmind

    Strength & \STR & Charisma & \CHA & Perception & \PRC \\
  
    Dexterity & \DEX & Manipulation & \MAN & Intelligence & \INT \\

    Stamina & \STM & Appearance & \APP & Wits & \WTS \\
  \else
    Strength & \STR & Dexterity & \DEX & Stamina & \STM \\

    Perception & \PRC & & & Wits & \WTS \\
  \fi
  \\
  \end{tabularx}

  \noindent
    \textbf{Abilities:} \ABIL

  \vspace{1em}

  \ifcainite
    \noindent
    \textbf{Disciplines:} \DISC
  \fi

  \noindent
  \begin{tabularx}{\textwidth}{@{}XX@{}}
  \if@mainmatter
  \\
    \textbf{Health} & \ifmind \textbf{Situation} \fi \\
    \healthBoxes{7} & \healthBoxes{7} \\
  \\
  \fi
  \textbf{Willpower} \arabic{WIL} & \expandafter\ifx\NAT\empty\else\textbf{Nature:} \NAT \fi \\
  \if@mainmatter
    \ifnum\value{WIL}>3
      \healthBoxes[3]{\value{WIL}}
    \fi & 
  \fi
  \ifmind
    \expandafter\ifx\NAT\empty \\ \else  \textbf{Demeanour:} \DEM \\ \fi 
  \else
    \\
  \fi
  \ifcainite
    \ROAD~\ROADRT & \textbf{Conscience} \CON \\
    \textbf{Self-Control} \SLF & \textbf{Courage} \CRG \\
  \fi
  \end{tabularx}

  \noindent
  \expandafter\ifx\CLT\empty%
  \else%
    \textbf{Culture: } \CLT
  \fi

  \expandafter\ifx\BACK\empty
  \else
    \noindent
    \textbf{Backgrounds: } \BACK
  \fi

  \expandafter\ifx\EQUIP\empty
  \else
    \noindent
    \textbf{Equipment: } \EQUIP
  \fi

  \end{tbox}

  \needspace{6em}
  \cleanupVars
}


\newcommand\animal[4]{
  \cainitefalse
  \renewcommand\characterName{#1}
  \body#2
  \animind#3
  \renewcommand\ABIL{#4}
  \characterBox
}


\newcommand\character[7]{
  \cainitefalse
  \renewcommand\characterName{#1}
  \body#2
  \mind#3
  \renewcommand\ABIL{#4}
  \archetypes#5
  \equipment#6
  \backgrounds#7
  
  \characterBox
}

\newcommand\vampire[8]{
  \mindtrue
  \cainitetrue
  \renewcommand\characterName{#1}
  \body#2
  \mind#3
  \renewcommand\ABIL{#4}
  \archetypes#5
  \equipment#6
  \backgrounds#7
  \vampireTraits#8
  
  \characterBox
}


%%%%% Character Sheet Commands %%%%%

\newcommand{\doublerule}[1][.4pt]{%
  \noindent
  \makebox[0pt][l]{\rule[.7ex]{\linewidth}{#1}}%
  \rule[.3ex]{\linewidth}{#1}}

\newcommand\csdiv[1]{\vspace{1em}\doublerule\par\headingfont\Large#1\par\doublerule\normalfont\normalsize}
\newcommand\csdots{\hrulefill\dottedSkill{0}}
\newcommand\csminititle[1]{\hrulefill\Large #1 \hrulefill \normalsize}

\newcommand\csSk[1][]{#1 \csdots\par}

\title{Draft}

\makeindex

\endinput
