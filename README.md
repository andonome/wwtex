This repo is a LaTeX style template for World of Darkness books.
It can produce these styles:

- *Vampire: the Masquerade* early editions.
- *Vampire: the Masquerade* later editions.
- *Dark Ages: Vampire* early editions.

Dependencies
============

- `inkscape`
- `latexmk`
- `lualatex`
- `make`

It requires these tlmgr packages:

`enumitem xstring multirow mathabx background everypage bigfoot fmtcount`
