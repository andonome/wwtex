include vars

TEST_PDFS = $(patsubst %.tex, %.pdf,$(wildcard tests/test*.tex))
.PHONY: test
test: $(TEST_PDFS) ## Compile test pdfs.

clean: ## Delete generated files.
	$(RM) -r *.pdf \
	svg-inkscape/ \
	dross/

